const { FuseBox, WebIndexPlugin, SassPlugin, CSSResourcePlugin, CSSPlugin, JSONPlugin, QuantumPlugin, Sparky } = require("fuse-box");
const fuse = FuseBox.init({
  sourceMaps: true,
  homeDir: "src",
  target: "browser@es6",
  output: "dist/$name.js",
  plugins: [
    this.isProduction &&
    QuantumPlugin({
      uglify: true,
      treeshake: true,
      bakeApiIntoBundle: "app",
    }),
    WebIndexPlugin({
      charset: 'utf-8',
      title: 'Gladoimhaven'
    }),
    SassPlugin(),
    CSSResourcePlugin({ dist: "dist/css-resources" }),
    CSSPlugin(),
    JSONPlugin(),
  ],
});
fuse.dev({port: 8081});
fuse
  .bundle("app")
  .instructions(" > main.ts")
  .hmr()
  .watch();
  

Sparky.task("clean", () => {
  return Sparky.src("dist").clean("dist");
});

Sparky.task("watch:images", () => {
  return Sparky.watch("**/*.+(svg|png|jpg|gif)", { base: "./assets/images" })
    .dest("./dist/images");
});

Sparky.task("default", ["clean", "watch:images"], () => {
  fuse.run();
});

Sparky.task("test", () => {
  fuse.bundle("app").test("[**/**.test.ts]");
});
