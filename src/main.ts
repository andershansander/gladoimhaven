import * as PIXI from 'pixi.js'
import { create as createBackground } from './graphics/background';
import { create as createMonster } from './graphics/monster';

if (!app) {

    var app = new PIXI.Application();
    document.body.appendChild(app.view);
    app.stage.addChild(createBackground(app.screen.width, app.screen.height));
    
    app.stage.addChild(createMonster(50, 50));
}

/*

button
    // Mouse & touch events are normalized into
    // the pointer* events for handling different
    // button events.
        .on('pointerdown', onButtonDown)
        .on('pointerup', onButtonUp)
        .on('pointerupoutside', onButtonUp)
        .on('pointerover', onButtonOver)
        .on('pointerout', onButtonOut);

        // Use mouse-only events
        // .on('mousedown', onButtonDown)
        // .on('mouseup', onButtonUp)
        // .on('mouseupoutside', onButtonUp)
        // .on('mouseover', onButtonOver)
        // .on('mouseout', onButtonOut)

        // Use touch-only events
        // .on('touchstart', onButtonDown)
        // .on('touchend', onButtonUp)
        // .on('touchendoutside', onButtonUp)

    // add it to the stage
    app.stage.addChild(button);

    // add button to array
    buttons.push(button);
    */