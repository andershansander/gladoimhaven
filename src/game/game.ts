
import * as types from './types';

export function tick(game: types.Game, delta: number) {
    
}

export function createGame(): types.Game {
    const playerPos = p(5, 5);
    return {
        stage: {
            players: [
                {
                    id: 1,
                    monsters: [
                        {
                            pos: {
                                x: 1,
                                y: 1
                            },
                            id: 1
                        }
                    ]
                }
            ]
        }
    };
}

export const controls = {
    
}

function p(x, y): types.Point {
    return { x, y };
}
