import { CardType, Card, CardInstance } from './types';

let idCounter = 0;

type CardRecord = Record<CardType, Card>;

const cards: CardRecord = {
    [CardType.Move]: {
        type: CardType.Move,
        text: 'Move'
    },
    [CardType.Attack]: {
        type: CardType.Attack,
        text: 'Move'
    }
};

export function generateCard(type: CardType): CardInstance {
    idCounter++;
    return {
        id: idCounter,
        card: cards[type]
    }
}