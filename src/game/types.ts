export type MonsterId = number;
export type PlayerId = number;
export type CardId = number;

export enum CardType {
    Move = 1,
    Attack = 2
}

export interface Card {
    type: CardType,
    text: string,
}

export interface CardInstance {
    id: CardId,
    card: Card
}

export const Cards: Card[] = [

]

export interface Point {
    x: number,
    y: number,
}

export interface Monster {
    id: MonsterId
    pos: Point,
}

export interface Player {
    id: PlayerId
    monsters: Monster[],
}

export interface Stage {
    players: Player[]
}

export interface Game {
    stage: Stage
}