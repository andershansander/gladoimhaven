import * as PIXI from 'pixi.js';


export function create(width: number, height: number) {
    const bg = PIXI.Sprite.fromImage('images/particle.png');
    bg.width = width;
    bg.height = height;
    return bg;
}