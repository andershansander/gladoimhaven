import * as PIXI from 'pixi.js';
import { textures } from './textures';

export function create(x, y) {
    const monster = new PIXI.Sprite(textures.monster);
    monster.anchor.set(0.5);
    monster.x = x
    monster.y = y;
    return monster;
}