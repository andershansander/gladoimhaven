interface Key {
  code: number,
  isDown: boolean,
  isUp: boolean,
  downHandler: (event) => void,
  upHandler: (event) => void,
  press?: () => void,
  release?: () => void,
}

export function createKey(keyCode): Key {
  const key: Key = {
    code: keyCode,
    isDown: false,
    isUp: true,
    downHandler: undefined,
    upHandler: undefined,
  };
  key.downHandler = event => {
    if (key.isUp && key.press) {
      key.press();
    }
    key.isDown = true;
    key.isUp = false;
    event.preventDefault();
  };
  key.upHandler = event => {
    if (event.keyCode === key.code) {
      if (key.isDown && key.release) {
        key.release();
      }
      key.isDown = false;
      key.isUp = true;
    }
    event.preventDefault();
  };
  return key;
}

function triggerHandler(keys: Key[], handler: string, event: KeyboardEvent) {
  const clickedKey = keys.find(key => key.code === event.keyCode);
  clickedKey && clickedKey[handler](event);
}

export function registerListeners(keys: Key[]) {
  window.addEventListener(
    "keydown", triggerHandler.bind(null, keys, 'downHandler'), false
  );
  window.addEventListener(
    "keyup", triggerHandler.bind(null, keys, 'upHandler'), false
  );
}